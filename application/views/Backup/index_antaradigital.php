<!DOCTYPE html>
<html lang="en">
<script src="assets/js/autoScrollTo.js"></script>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Antara Digital</title>

    <!-- Favicon -->
    <link rel="icon" href="assets/images/Favicon.ico" type="image/x-icon" />
    <!-- Bootstrap CSS -->
    <link href="assets/css/bootstrap.min.css" rel="stylesheet">
    <!-- Animate CSS -->
    <link href="assets/vendors/animate/animate.css" rel="stylesheet">
    <!-- Icon CSS-->
	  <link rel="stylesheet" href="assets/vendors/font-awesome/css/font-awesome.min.css">
    <!-- Camera Slider -->
    <link rel="stylesheet" href="assets/vendors/camera-slider/camera.css">
    <!-- Owlcarousel CSS-->
	<link rel="stylesheet" type="text/css" href="assets/vendors/owl_carousel/owl.carousel.css" media="all">

    <!--Theme Styles CSS-->
	<link rel="stylesheet" type="text/css" href="assets/css/style.css" media="all" />

    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="assets/js/html5shiv.min.js"></script>
    <script src="assets/js/respond.min.js"></script>
    <![endif]-->
</head>
<body>
    <!-- Preloader -->
    <div class="preloader"></div>


	<!-- Header_Area -->
    <nav class="navbar navbar-default header_aera" id="main_navbar">
        <div class="container">
            <!-- searchForm -->
            <div class="searchForm">
                <form action="#" class="row m0">
                    <div class="input-group">
                        <span class="input-group-addon"><i class="fa fa-search"></i></span>
                        <input type="search" name="search" class="form-control" placeholder="Type & Hit Enter">
                        <span class="input-group-addon form_hide"><i class="fa fa-times"></i></span>
                    </div>
                </form>
            </div><!-- End searchForm -->
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="col-md-2 p0">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#min_navbar">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="#" onclick="return false; "onmouseup="resetScroller('Home');" ><img src="assets/images/LogoAntara.png" alt="Logo Antara" ></a>
                </div>
            </div>

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="col-md-10 p0">
                <div class="collapse navbar-collapse" id="min_navbar">
                    <ul class="nav navbar-nav navbar-right">

                        <li class="dropdown submenu">
                            <a href='#' class="" onclick="return false;" onmousedown="autoScrollTo('about');"onmouseup="resetScroller('about');">About Us</a>
                        </li>

                        <li class="dropdown submenu">
                            <a href="#" class="" onclick="return false;" onmousedown="autoScrollTo('Service');"onmouseup="resetScroller('Service');">Services</a>

                        </li>
                        <li class="dropdown submenu"><a href="#" class="" onclick="return false;" onmousedown="autoScrollTo('Client');"onmouseup="resetScroller('Client');" >Our Client</a></li>
                        <li class="dropdown submenu">
                            <a href="#" class="" onclick="return false;" onmousedown="autoScrollTo('News');"onmouseup="resetScroller('News');">News</a>
                        </li>
                        <li class="dropdown submenu"><a href="#" class="" onclick="return false;" onmousedown="autoScrollTo('Contact');">Contact Us</a></li>
                    </ul>
                </div><!-- /.navbar-collapse -->
            </div>
        </div><!-- /.container -->
    </nav>
	<!-- End Header_Area -->

    <!-- Slider area -->
    <section class="slider_area row m0" id='Home'>
        <div class="slider_inner">
            <div data-thumb="images/slider-1.jpg" data-src="assets/images/slider-1.jpg" >
                <div class="camera_caption">
                   <div class="container">
                        <h5 class=" wow fadeInUp animated"></h5>
                        <h3 class=" wow fadeInUp animated" data-wow-delay="0.5s"></h3>
                        <a class=" wow fadeInUp animated" data-wow-delay="1s" href="#"> </a>
                   </div>
                </div>
            </div>
            <div data-thumb="images/slider-2.jpg" data-src="assets/images/slider-2.jpg">
                <div class="camera_caption">
                   <div class="container">
                        <h5 class=" wow fadeInUp animated"></h5>
                        <h3 class=" wow fadeInUp animated" data-wow-delay="0.5s"></h3>
                        <a class=" wow fadeInUp animated" data-wow-delay="1s" href="#"> </a>
                   </div>
                </div>
            </div>
            <div data-thumb="images/slider-3.jpg" data-src="assets/images/slider-3.jpg">
                <div class="camera_caption">
                   <div class="container">
                        <h5 class=" wow fadeInUp animated"></h5>
                        <h3 class=" wow fadeInUp animated" data-wow-delay="0.5s"></h3>
                        <a class=" wow fadeInUp animated" data-wow-delay="1s" href="#"> </a>
                   </div>
                </div>
            </div>
        </div>
    </section>
    <!-- End Slider area -->
    <br>
    <div class="container">
        <div class="media-container-row">
            <div class="title col-12 col-md-8">
                <h2 class="Why_area display-1"><br>KENAPA BERIKLAN DI ANTARA DIGITAL MEDIA?<br></h2>



            </div>
        </div>
    </div>

    <style>
    .display-1{font: 700 75px  'Quicksand', sans-serif;line-height:1.1;color: red; position: center }
    .Why_area {
      background: #ffffff;
      text-align: left;
      position: relative;
      padding-bottom: 50px;

    }

    </style>
    <!-- Our Achievements Area -->

    <section class="our_achievments_area" data-stellar-background-ratio="0.3" id='Archiment'>
        <div class="container">
            <div class="tittle wow fadeInUp">

            </div>
            <div class="achievments_row row">
                <div class="col-md-4 col-sm-6 p0 completed">
                    <i class="" aria-hidden="true" ><img src="assets/images/icon outdoor white.png"></i>
                    <span class="counter">1000</span>
                    <h6>Indoor</h6>
                </div>
                <div class="col-md-4 col-sm-6 p0 completed">
                    <i class="" aria-hidden="true"><img src="assets/images/icon indoor white.png"></i>
                    <span class="counter">2500</span>
                    <h6>Outdoor</h6>
                </div>
                <div class="col-md-4 col-sm-6 p0 completed">
                    <i class="" aria-hidden="true"><img src="assets/images/icon home white.png"></i>
                    <span class="counter">4500</span>
                    <h6>Home</h6>
                </div>
            </div>
        </div>
          <br><br><br><br>
    </section>

    <!-- End Our Achievments Area -->

    <!-- About Us Area -->

    <section class="about_us_area row"  id="about">
        <div class="container">
            <div class="tittle wow fadeInUp">
                <h2>ABOUT US</h2>
                <h4>Antara Digital</h4>
            </div>
            <div class="row about_row">
                <div class="who_we_area col-md-7 col-sm-6">
                    <div class="subtittle">
                </div>
                    <H5> <p>ANTARA DIGITAL MEDIA
                      Anak perusahaan Perum LKBN ANTARA yang bergerak di bisnis media digital dan bertugas menyebarkan informasi dan berita dari Kantor Berita ANTARA kepada masyarakat di seluruh Indonesia dalam format teks, foto dan video.
                      ANTARA DIGITAL MEDIA memiliki layanan digital yang tak hanya bermanfaat untuk mensosialisasikan program dan kebijakan pemerintah serta perusahaan BUMN, melainkan juga dapat menjadi media promosi pariwisata dan kebudayaan.
                      ANTARA DIGITAL MEDIA juga menawarkan layanan yang dapat menjadi solusi bisnis bagi perusahaan yang ingin mengkampanyekan atau mempromosikan produk secara komersial. Tersedia layanan media dalam ruang (indoor) dalam bentuk TVC dan media luar ruang (outdoor) berupa digital billboard yang tersebar di sejumlah area publik. Juga ditawarkan program TV digital ke rumah (home digital TV).</p> </H5>

                </div>
                <div class="col-md-5 col-sm-6 about_client">
                    <img src="assets/images/about.png" alt="">
                </div>

            </div>

        </div>

    </section>
    <!-- End About Us Area -->
    <!-- Our Services Area -->
    <section class="our_services_area" id="Service">
        <div class="container">
            <div class="tittle wow fadeInUp">
                <h2>Our Services</h2>
                <h4>Antara Digital</h4>
            </div>
            <div class="portfolio_inner_area">
               <div class="portfolio_item">
                   <div class="grid-sizer"></div>

                    <div class="single_facilities col-xs-4 p0 webdesign">
                       <div class="single_facilities_inner">
                            	<img src="assets/images/indoor.jpg" alt="">
                            	<a id="button" href="<?php echo site_url('user/ourServices/indoor'); ?>">
                            		<div class="gallery_hover">
                                 <h4>Indoor</h4>
                                 <h5>Media interaktif dalam bentuk TV Display (plasma atau LCD TV) yang menyajikan informasi secara Real-time dalam format text, video dan foto</h5>
                            		</div>
              			</a>
                        </div>
                    </div>

                    <div class="single_facilities col-xs-4 p0 adversting webdesign adversting">
                       <div class="single_facilities_inner">
                            <img src="assets/images/outdoor.jpg" alt="">
                            <a id="button" href="<?php echo site_url('user/ourServices/outdoor'); ?>">
                            <div class="gallery_hover">
                            <h4>Outdoor</h4>
                            <h5>O-Media adalah media digital luar ruang berupa videotron berbasis internet yang menampilkan informasi berbentuk teks, foto dan video.</h5>
                            </div>
                          </a>
                        </div>
                    </div>

                    <div class="single_facilities col-xs-4 p0 webdesign photography magazine adversting">
                       <div class="single_facilities_inner">
                            <img src="assets/images/home.jpg" alt="">
                            <a id="button" href="<?php echo site_url('user/ourServices/home'); ?>">
                            <div class="gallery_hover">
                            <h4>Home</h4>
                            <h5>home home home home home home home home home home home</h5>
                            </div>
                          </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>




    <!-- End Our Services Area -->

    <!-- Our Partners Area starts here -->
    <section class="our_partners_area" id='Client'>
        <div class="container">
            <div class="tittle wow fadeInUp">
                <h2>Our Client</h2>
                <h4>Antara Digital</h4>
            </div>
            <br><br><br>
            <center>
            <div class="w3-content w3-section" style="max-width:777px">
              <img class="mySlides" src="assets/images/client_logo/Logo1.png" style="width:100%">
              <img class="mySlides" src="assets/images/client_logo/Logo2.png" style="width:100%">
              <img class="mySlides" src="assets/images/client_logo/Logo3.png" style="width:100%">
            </div>
          </center>
        </div>
          <br><br><br><br><br><br><br><br><br><br><br><br><br>
</section>
    <!-- End Our Partners Area -->
    <style>
    .mySlides {display:none;}
    </style>
    <script>
    var myIndex = 0;
    carousel();

    function carousel() {
        var i;
        var x = document.getElementsByClassName("mySlides");
        for (i = 0; i < x.length; i++) {
           x[i].style.display = "none";
        }
        myIndex++;
        if (myIndex > x.length) {myIndex = 1}
        x[myIndex-1].style.display = "block";
        setTimeout(carousel, 5555); // Change image every 2 seconds
    }
    </script>

<!-- What NEWS Area -->
<style>
.movearea {
   position: relative;
}

.slick-slide {
   margin: 0px 20px;
   background:red;
}

.slick-slide img {
   width: 100%;
}

.slick-slider {
   position: relative;
   display: block;
   box-sizing: border-box;
   -webkit-user-select: none;
   -moz-user-select: none;
   -ms-user-select: none;
   user-select: none;
   -webkit-touch-callout: none;
   -khtml-user-select: none;
   -ms-touch-action: pan-y;
   touch-action: pan-y;
   -webkit-tap-highlight-color: transparent;
   background:grey;
}

.slick-list {
   position: relative;
   display: block;
   overflow: hidden;
   margin: 0;
   padding: 0;
}
.slick-list:focus {
   outline: none;
}
.slick-list.dragging {
   cursor: pointer;
   cursor: hand;
}

.slick-slider .slick-track,
.slick-slider .slick-list
{
    -webkit-transform: translate3d(0, 0, 0);
    -moz-transform: translate3d(0, 0, 0);
    -ms-transform: translate3d(0, 0, 0);
    -o-transform: translate3d(0, 0, 0);
    transform: translate3d(0, 0, 0);
}

.slick-track
{
    position: relative;
    top: 0;
    left: 0;
    display: block;
}
.slick-track:before,
.slick-track:after
{
    display: table;
    content: '';
}
.slick-track:after
{
    clear: both;
}
.slick-loading .slick-track
{
    visibility: hidden;
}

.slick-slide
{
    display: none;
    float: left;
    height: 100%;
    min-height: 1px;
}
[dir='rtl'] .slick-slide
{
    float: right;
}
.slick-slide img
{
    display: block;
}
.slick-slide.slick-loading img
{
    display: none;
}
.slick-slide.dragging img
{
    pointer-events: none;
}
.slick-initialized .slick-slide
{
    display: block;
}
.slick-loading .slick-slide
{
    visibility: hidden;
}
.slick-vertical .slick-slide
{
    display: block;
    height: auto;
    border: 1px solid transparent;
}
.slick-arrow.slick-hidden {
    display: none;
}
</style>
<section class="what_we_area row" id='News'>
    <div class="container">
        <div class="tittle wow fadeInUp">
          <h2><font color="#ffffff"><h1>News</h1></font></h2>
          <h4>Antara Digital</h4>
      </div>
      <div class="news row construction_iner" style="overflow:hidden">
       <?php for ($i = 1; $i <= $this->session->userdata('total_news'); $i++) { ?>
          <div class="construction item" style="width: 350px;">
            <div class="movearea" id="obj<?php echo $i;?>">
                <div class="cns-img">
                    <img src="assets/images/Antara.png" alt="">
                </div>
                <br>
                <div class="cns-content" style="height: 150px;overflow: hidden;">
                    <a href="#"><?php echo $this->session->userdata['news']['judul'][$i]; ?></a>
                    <p style="text-align: justify;"><?php echo $this->session->userdata['news']['deskripsi'][$i]; ?></p>
                </div>
            </div>
        </div>
    <?php } ?>
</div>
<script type="text/javascript">
   var totalNews = "<?php echo $this->session->userdata('total_news'); ?>";
</script>
<!-- <script type="text/javascript" src="/assets/js/newsSlider.js"></script> -->
</div>
<br><br><br><br><br>
</section>
<!-- End NEWS Area -->

        <!-- All contact Info -->
        <section class="all_contact_info" id='Contact'>
            <div class="container">


                <div class="row contact_row">

                    <div class="col-sm-6 contact_info">
                        <h2>Contact Info</h2>
                        <p></p>
                          <h3><b>Management of PT. Antara Digital Multimedia Utama</b></h3><br>
                          <div><h4>Darmadi,</h4><h6> Chief Executive Officer</h6></div><br>
                          <div><h4>Jonny Zulkarnain,</h4><h6> Head of Sales and Marketing Division</h6></div><br>
                          <div><h4>Tutus Wibowo,<h6> Head of Information Technology and Media Division</h6></div><br>
                          <div><h4>Purnama Saidina Abubakar,<h6> Human Capital and Finance Division</h6></div>

                          <div class="location">
                              <div class="location_laft">
                                  <p class="f_location" >location</p>
                                  <p >phone</p>
                                  <p >fax</p>
                                  <p >email<br><br></p>
                              </div>

                              <div class="address">
                                  <p> WISMA ANTARA Lt. 7 <br> JL. Medan Merdeka Selatan No. 17
                                  Jakarta Pusat, DKI Jakarta, Indonesia</p>
                                  <p >+62 21 3865323 ; +62 21 3865325</p>
                                  <p >+62 21 3865327</p>
                                  <p>Marketing: marketing@imq21.com<br>
                                  Sales: sales@imq21.com<br>
                                  Customer Service: cs@imq21.com</p></h4>
                              </div>
                            </div>
                    </div>
                    <div class="col-sm-6 contact_info send_message">
                        <h2>Send Us a Message</h2>
                        <form action="#" class="form-inline contact_box" method="post" >
                            <input type="text" class="form-control input_box" placeholder="First Name *" name="input_contact_us_first_name">
                            <input type="text" class="form-control input_box" placeholder="Last Name *"  name="input_contact_us_last_name">
                            <input type="text" class="form-control input_box" placeholder="Your Email *" name="input_contact_us_email">
                            <input type="text" class="form-control input_box" placeholder="Subject"      name="input_contact_us_subject">
                            <input type="text" class="form-control input_box" placeholder="Your Website" name="input_contact_us_website">
                            <textarea class="form-control input_box" placeholder="Message"></textarea>
                            <button type="submit" class="btn btn-default">Send Message</button>
                        </form>
                    </div>
                </div>
            </div>
        </section>
        <!-- End All contact Info -->

    <!-- Footer Area -->
    <footer class="footer_area">

        <div class="copyright_area">
            Copyright 2018 All rights reserved. Designed by <a href="#">Antara Digital</a>
        </div>
    </footer>
    <!-- End Footer Area -->

    <!-- jQuery JS -->
    <script src="assets/js/jquery-1.12.0.min.js"></script>
    <!-- Bootstrap JS -->
    <script src="assets/js/bootstrap.min.js"></script>
    <!-- Animate JS -->
    <script src="assets/vendors/animate/wow.min.js"></script>
    <!-- Camera Slider -->
    <script src="assets/vendors/camera-slider/jquery.easing.1.3.js"></script>
    <script src="assets/vendors/camera-slider/camera.min.js"></script>
    <!-- Isotope JS -->
    <script src="assets/vendors/isotope/imagesloaded.pkgd.min.js"></script>
    <script src="assets/vendors/isotope/isotope.pkgd.min.js"></script>
    <!-- Progress JS -->
    <script src="assets/vendors/Counter-Up/jquery.counterup.min.js"></script>
    <script src="assets/vendors/Counter-Up/waypoints.min.js"></script>
    <!-- Owlcarousel JS -->
    <script src="assets/vendors/owl_carousel/owl.carousel.min.js"></script>
    <!-- Stellar JS -->
    <script src="assets/vendors/stellar/jquery.stellar.js"></script>
    <!-- Theme JS -->
    <script src="assets/js/theme.js"></script>


</body>
</html>
